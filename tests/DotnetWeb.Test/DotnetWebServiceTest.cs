using System;
using System.Threading.Tasks;
using DotnetWeb.Common;
using DotnetWeb.Core;
using DotnetWeb.Public;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NUnit.Framework;

namespace DotnetWeb.Test
{
    [TestFixture]
    public class DotnetWebServiceTest
    {
        private IDotnetWebService _service;
        private IDotnetWebRepository _repository;
        private ILogger<DotnetWebService> _logger;

        [SetUp]
        public void Setup()
        {
            _repository = Substitute.For<IDotnetWebRepository>();
            _logger = Substitute.For<ILogger<DotnetWebService>>();
            _service = new DotnetWebService(_repository, _logger);
        }

        [Test]
        public void GetWithValidInputReturnsValue()
        {
            //arrange
            var random = new Random();
            var id = random.Next(0, 1000);
            var expected = new DotnetWebResponse { Item1 = random.Next(0, 1000).ToString() };

            _repository.Get(id).Returns(Task.FromResult(expected));

            //act
            var result = _service.Get(id.ToString()).Result;

            //assert
            _repository.Received(1).Get(id);
            Assert.That(result, Is.EqualTo(expected));
        }
    }
}
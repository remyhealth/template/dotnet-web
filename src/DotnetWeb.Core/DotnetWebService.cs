using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotnetWeb.Common;
using DotnetWeb.Public;
using Microsoft.Extensions.Logging;

namespace DotnetWeb.Core
{
    public class DotnetWebService : IDotnetWebService
    {
        private readonly ILogger<DotnetWebService> _logger;
        private readonly IDotnetWebRepository _repository;

        public DotnetWebService(IDotnetWebRepository repository, ILogger<DotnetWebService> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        public async Task<DotnetWebResponse> Get(string id)
        {
            _logger.LogDebug("Get Service Hit");

            Validate(id);

            return await _repository.Get(int.Parse(id));
        }

        public Task<int> Create(DotnetWebRequest request)
        {
            _logger.LogDebug("Create Service Hit");

            return Task.FromResult(0);
        }

        public async Task<IList<DotnetWebResponse>> List()
        {
            _logger.LogDebug("List Service Hit");

            var list = await _repository.List();

            return list == null ? new List<DotnetWebResponse>() : list.ToList();
        }

        private void Validate(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new Exception("Missing id");
            }

            if (!int.TryParse(id, out var parsed))
            {
                throw new Exception("Invalid id");
            }
        }
    }
}
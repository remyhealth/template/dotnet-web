using System.Collections.Generic;
using System.Threading.Tasks;
using DotnetWeb.Public;

namespace DotnetWeb.Common
{
    public interface IDotnetWebRepository
    {
        Task<DotnetWebResponse> Get(int id);
        Task<IEnumerable<DotnetWebResponse>> List();
    }
}
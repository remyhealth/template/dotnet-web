using System.Collections.Generic;

namespace DotnetWeb.Common
{
    public class DotnetWebConstants
    {
        public const string ApplicationEnvironmentVariableName = "ASPNETCORE_ENVIRONMENT";
        public const string ApplicationName = "DotnetWeb";
        public const string DefaultLogLocation = @"D:\home\LogFiles\Application";
        public const string DatabaseEnvironmentVariableUrl = "DatabaseUrl";
        public const string DatabaseEnvironmentVariableUsername = "DatabaseUsername";
        public const string DatabaseEnvironmentVariablePassword = "DatabasePassword";
        public const string DatabaseEnvironmentVariableCatalog = "DotnetWebDatabaseCatalog";
        public const string ApiVersion1 = "1";
        public const string ApiVersion2 = "2";

        public static List<string> ApiList() => new List<string>
        {
            ApiVersion1,
            ApiVersion2
        };

        public static string CurrentApiVersion() => ApiVersion1;
    }
}
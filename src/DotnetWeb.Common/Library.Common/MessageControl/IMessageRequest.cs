using System;

namespace DotnetWeb.Common.Library.Common.MessageControl
{
    public interface IMessageRequest
    {
        Guid MessageId();
        Guid? CallerId();
        string ClientMessageId();
        string CallerAppId();
        string AppId();
        string IpAddress();
        DateTimeOffset StartTime();
        bool NoCache();
        bool IsCached();
        string GroupId();
        string ServiceType();
        ServiceTypeEnum ServiceTypeEnum();
        bool IsPortal();
        bool IsApi();
        bool IsBatch();
        string SearchKey();
        void SearchKey(string searchKey);
        bool ExcludeFromBilling();
        long? BatchId();
        Guid? TransactionId();
    }

}
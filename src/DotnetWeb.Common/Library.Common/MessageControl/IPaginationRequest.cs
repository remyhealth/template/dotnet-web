namespace DotnetWeb.Common.Library.Common.MessageControl
{
    public interface IPaginationRequest
    {
        int PageIndex { get; }
        int PageSize { get; }
    }
}
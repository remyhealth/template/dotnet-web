using System;

namespace DotnetWeb.Common.Library.Common.CacheControl
{
    public interface IInstanceCache
    {
        bool Exists(string key);
        T Get<T>(string key);
        void Add<T>(string key, T val, TimeSpan expires);
        void Remove(string key);
    }
}
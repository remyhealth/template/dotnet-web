namespace DotnetWeb.Common.Library.Common.DatabaseControl
{
    public interface IConnection
    {
        string ConnectionString { get; }
    }
}
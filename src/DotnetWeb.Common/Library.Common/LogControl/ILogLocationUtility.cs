namespace DotnetWeb.Common.Library.Common.LogControl
{
    public interface ILogLocationUtility
    {
        string GetLogLocation(string defaultLogLocation, string logName,
            string defaultLogLocationEnvironmentVariableName = "MY_LOG_LOCATION");
    }
}
using System;
using DotnetWeb.Common.Library.Common.EnvironmentControl;

namespace DotnetWeb.Common.Library.Common.LogControl
{
    public class LogLocationUtility : ILogLocationUtility
    {
        private readonly IEnvironmentVariableUtility _environmentVariableUtility;

        public LogLocationUtility(IEnvironmentVariableUtility environmentVariableUtility)
        {
            _environmentVariableUtility = environmentVariableUtility;
        }

        public string GetLogLocation(string defaultLogLocation, string logName, string defaultLogLocationEnvironmentVariableName = "MY_LOG_LOCATION")
        {
            if (string.IsNullOrWhiteSpace(defaultLogLocation))
            {
                throw new Exception("Missing Default Log Location");
            }

            var extensionName = @"\" + logName + ".log";
            try
            {
                var location = _environmentVariableUtility.Get(defaultLogLocationEnvironmentVariableName);

                return !string.IsNullOrWhiteSpace(location) ? location + extensionName : defaultLogLocation + extensionName;
            }
            catch
            {
                return defaultLogLocation + extensionName;
            }
        }
    }
}
namespace DotnetWeb.Common.Library.Common.EnvironmentControl
{
    public class CustomHostingEnvironment : ICustomHostingEnvironment
    {
        public CustomHostingEnvironment(string applicationName, EnvironmentEnum environmentName)
        {
            Environment = environmentName;
            ApplicationName = applicationName;
        }

        public EnvironmentEnum Environment { get; set; }

        public string ApplicationName { get; }
    }
}
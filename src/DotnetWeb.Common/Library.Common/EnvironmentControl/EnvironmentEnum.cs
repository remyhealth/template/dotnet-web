namespace DotnetWeb.Common.Library.Common.EnvironmentControl
{
    public enum EnvironmentEnum
    {
        Unknown = 0,
        Local = 1,
        Development = 2,
        Uat = 3,
        Staging = 4,
        Production = 5
    }
}
namespace DotnetWeb.Common.Library.Common.EnvironmentControl
{
    public interface IEnvironmentVariableUtility
    {
        string Get(string key);
        string SafeGet(string key, string defaultValue = null);
        void Set(string key, string value);
    }
}
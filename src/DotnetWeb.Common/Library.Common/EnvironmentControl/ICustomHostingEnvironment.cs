namespace DotnetWeb.Common.Library.Common.EnvironmentControl
{
    public interface ICustomHostingEnvironment
    {
        EnvironmentEnum Environment { get; }
        string ApplicationName { get; }
    }
}
namespace DotnetWeb.Common.Library.Common.EnvironmentControl
{
    public class ApplicationIdUtility : IApplicationIdUtility
    {
        public string GetApplicationId(string applicationId)
        {
            if (string.IsNullOrWhiteSpace(applicationId))
            {
                return applicationId;
            }

            var loweredAppId = applicationId.ToLower();

            if (loweredAppId.Contains("prod"))
            {
                return loweredAppId.Replace("prod", "");
            }

            if (loweredAppId.Contains("staging"))
            {
                return loweredAppId.Replace("staging", "");
            }

            if (loweredAppId.Contains("test"))
            {
                return loweredAppId.Replace("test", "");
            }

            if (loweredAppId.Contains("uat"))
            {
                return loweredAppId.Replace("uat", "");
            }

            return loweredAppId.Contains("dev") ? loweredAppId.Replace("dev", "") : loweredAppId;
        }

    }
}
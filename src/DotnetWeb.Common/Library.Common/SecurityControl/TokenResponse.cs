using System.Collections.Generic;

namespace DotnetWeb.Common.Library.Common.SecurityControl
{
    public class TokenResponse
    {
        public IList<TokenResponseItem> Responses { get; set; }
    }
}
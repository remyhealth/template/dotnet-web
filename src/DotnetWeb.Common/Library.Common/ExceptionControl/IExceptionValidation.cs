using System.Collections.Generic;
using System.Net;

namespace DotnetWeb.Common.Library.Common.ExceptionControl
{
    public interface IExceptionValidation
    {
        void AddError(ValidationError validationError);
        void AddErrors(IEnumerable<ValidationError> validationErrors);
        bool IsValid();
        ApiException Throw(HttpStatusCode httpStatusCode, string myId);
        ApiException Throw(HttpStatusCode httpStatusCode, string myId, string clientMessageId);
        List<ValidationError> GetErrors();
    }

}
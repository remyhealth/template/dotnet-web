using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace DotnetWeb.Common.Library.Common.ExceptionControl
{
    public class ExceptionValidation : IExceptionValidation
    {
        private readonly List<ValidationError> _validationErrors;

        public ExceptionValidation()
        {
            _validationErrors = new List<ValidationError>();
        }

        public void AddError(ValidationError validationError)
        {
            if (validationError != null)
            {
                _validationErrors.Add(validationError);
            }
        }

        public void AddErrors(IEnumerable<ValidationError> validationErrors) => _validationErrors.AddRange(validationErrors);

        public bool IsValid() => !_validationErrors.Any();

        public List<ValidationError> GetErrors() => _validationErrors;

        public ApiException Throw(HttpStatusCode httpStatusCode, string myId) => new ApiException(httpStatusCode, myId, _validationErrors);

        public ApiException Throw(HttpStatusCode httpStatusCode, string myId, string clientMessageId) => new ApiException(httpStatusCode, myId, _validationErrors, clientMessageId);
    }
}
using DotnetWeb.Common.Library.Common.EnvironmentControl;

namespace DotnetWeb.Common.Library.Common.ProjectControl
{
    public interface IProjectInformationMapper
    {
        ICurrentProjectInformation MapCurrent(AppSettings appSettings, string currentApiVersion,
            EnvironmentEnum environment = EnvironmentEnum.Unknown);

        IHealthProjectInformation MapHealth(ICurrentProjectInformation currentProjectInformation);
    }
}
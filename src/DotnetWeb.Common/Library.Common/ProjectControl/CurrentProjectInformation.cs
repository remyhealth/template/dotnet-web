namespace DotnetWeb.Common.Library.Common.ProjectControl
{
    public class CurrentProjectInformation : ICurrentProjectInformation
    {
        public string ApplicationName { get; set; }
        public ICurrentVersionInformation VersionInformation { get; set; }
        public ICurrentEnvironmentInformation CurrentEnvironment { get; set; }
    }
}
namespace DotnetWeb.Common.Library.Common.ProjectControl
{
    public interface ICurrentThirdPartyInformation
    {
        string Audience { get; set; }
        string Issuer { get; set; }
        string Key { get; set; }
        string Domain { get; set; }
        string TenantId { get; set; }
        string ClientId { get; set; }
    }
}
using DotnetWeb.Common.Library.Common.EnvironmentControl;

namespace DotnetWeb.Common.Library.Common.ProjectControl
{
    public interface IHealthEnvironmentInformation
    {
        string Url { get; set; }
        EnvironmentEnum Environment { get; set; }
        string Name { get; set; }
    }
}
using DotnetWeb.Common.Library.Common.EnvironmentControl;

namespace DotnetWeb.Common.Library.Common.ProjectControl
{
    public class HealthEnvironmentInformation : IHealthEnvironmentInformation
    {
        public string Url { get; set; }
        public EnvironmentEnum Environment { get; set; }
        public string Name { get; set; }
    }
}
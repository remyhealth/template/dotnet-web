namespace DotnetWeb.Common.Library.Common.ProjectControl
{
    public class EnvironmentInformation : IEnvironmentInformation
    {
        public ICurrentEnvironmentInformation Production { get; set; }
        public ICurrentEnvironmentInformation Staging { get; set; }
        public ICurrentEnvironmentInformation Uat { get; set; }
        public ICurrentEnvironmentInformation Development { get; set; }
        public ICurrentEnvironmentInformation Local { get; set; }
    }
}
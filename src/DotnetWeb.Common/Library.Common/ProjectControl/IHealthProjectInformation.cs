namespace DotnetWeb.Common.Library.Common.ProjectControl
{
    public interface IHealthProjectInformation : IProjectInformation
    {
        IHealthEnvironmentInformation CurrentEnvironment { get; set; }
    }
}
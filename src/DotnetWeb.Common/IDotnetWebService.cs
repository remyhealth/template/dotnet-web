using System.Collections.Generic;
using System.Threading.Tasks;
using DotnetWeb.Public;

namespace DotnetWeb.Common
{
    public interface IDotnetWebService
    {
        Task<DotnetWebResponse> Get(string id);
        Task<int> Create(DotnetWebRequest request);
        Task<IList<DotnetWebResponse>> List();
    }
}
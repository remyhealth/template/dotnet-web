using System;
using System.IO;
using System.Security.Claims;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using DotnetWeb.Bootstrapper.Library.Bootstrapper;
using DotnetWeb.Common;
using DotnetWeb.Common.Library.Common.EnvironmentControl;
using DotnetWeb.Common.Library.Common.ProjectControl;
using DotnetWeb.IIS.Library.IIS;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DotnetWeb.IIS
{
    public class Startup
    {
        public IHostingEnvironment HostingEnvironment { get; }
        public IConfiguration Configuration { get; }
        public IContainer ApplicationContainer { get; private set; }

        public Startup(IConfiguration config, IHostingEnvironment env)
        {
            HostingEnvironment = env;
            Configuration = config;
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            var projectInformation = GetCurrentProjectInformation(DotnetWebConstants.CurrentApiVersion(), new EnvironmentVariableUtility());
            var healthInformation = new ProjectInformationMapper().MapHealth(projectInformation);

            services
                .AddCors(new CustomCors(projectInformation).Get())
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddControllersAsServices()
                .AddXmlSerializerFormatters();

            services.AddSingleton(projectInformation);
            services.AddSingleton(healthInformation);
            services.AddApiVersioning(new CustomApiVersioning().Get(projectInformation.VersionInformation.ApiVersion));
            services.AddSwaggerGen(new CustomSwaggerGen(projectInformation.ApplicationName, projectInformation.CurrentEnvironment.Name).Get(DotnetWebConstants.ApiList()));
            services.AddHttpContextAccessor();
            services.AddScoped<ClaimsPrincipal>(provider =>
            {
                var context = provider.GetService<IHttpContextAccessor>()?.HttpContext;
                if (context == null)
                {
                    return new ClaimsPrincipal(new ClaimsIdentity());
                }

                if (context.User == null)
                {
                    return new ClaimsPrincipal(new ClaimsIdentity());
                }

                return context.User;
            });

            services.AddAuthentication(options =>
                {
                    options.DefaultScheme = "Basic";
                    options.DefaultAuthenticateScheme = "Basic";
                    options.DefaultChallengeScheme = "Basic";
                }).AddJwtBearer(options =>
                {
                    options.Authority = "https://" + projectInformation.CurrentEnvironment.Authentication.Domain + "/";
                    options.Audience = projectInformation.CurrentEnvironment.Authentication.Audience;
                }).AddBasic(new CustomBasicAuthentication(projectInformation).Get());

            HostingEnvironment.ApplicationName = projectInformation.ApplicationName;
            HostingEnvironment.EnvironmentName = projectInformation.CurrentEnvironment.Name;

            ApplicationContainer = new BootstrapperRegistration()
                .RegisterAutofac(services)
                .Build();

            return new AutofacServiceProvider(ApplicationContainer);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            var useDevelopmentSettings = env.IsDevelopment() || env.IsEnvironment(Enum.GetName(typeof(EnvironmentEnum), EnvironmentEnum.Local));

            app.UseCors(CustomCors.PolicyName);
            app.UseSwagger();

            if (useDevelopmentSettings)
            {
                app.UseSwaggerUI(c =>
                {
                    c.OAuthUseBasicAuthenticationWithAccessCodeGrant();
                    c.RoutePrefix = string.Empty;
                    foreach (var apiVersion in DotnetWebConstants.ApiList())
                    {
                        c.SwaggerEndpoint($"/swagger/v{apiVersion}/swagger.json", $"DotnetWeb V{apiVersion}");
                    }
                });
            }

            app.UseAuthentication();
            app.UseArchiveMiddleware();
            app.UseMvc();
            app.UseApiVersioning();

            if (useDevelopmentSettings)
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
                app.UseHttpsRedirection();
            }
        }

        private IConfiguration GetConfigurationRoot(IConfiguration configuration = null)
        {
            if (configuration != null)
            {
                return new ConfigurationBuilder()
                    .AddConfiguration(configuration)
                    .AddJsonFile("appsettings.json", false, false)
                    .AddEnvironmentVariables().Build();
            }

            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, false)
                .AddEnvironmentVariables().Build();
        }
        private ICurrentProjectInformation GetCurrentProjectInformation(string currentApiVersion, IEnvironmentVariableUtility environmentVariableUtility)
        {
            var appSettings = GetConfigurationRoot()
                .GetSection(AppSettings.AppSettingsSectionName)
                .Get<AppSettings>();

            var environment = new CustomHostingEnvironmentMapper(appSettings.ApplicationName, environmentVariableUtility).Get();

            return new ProjectInformationMapper().MapCurrent(appSettings, currentApiVersion, environment.Environment);
        }
    }
}
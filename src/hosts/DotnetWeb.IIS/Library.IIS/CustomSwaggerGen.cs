using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using DotnetWeb.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace DotnetWeb.IIS.Library.IIS
{
    public class CustomSwaggerGen
    {
        private readonly string _applicationName;
        private readonly string _environmentName;

        public CustomSwaggerGen(string applicationName, string environmentName)
        {
            _applicationName = applicationName;
            _environmentName = environmentName;
        }

        public Action<SwaggerGenOptions> Get(List<string> currentProjectApiList) =>
            options =>
            {
                foreach (var apiVersion in currentProjectApiList)
                {
                    options.SwaggerDoc($"v{apiVersion}", GetOpenApiDocument(apiVersion));
                }

                options.AddSecurityDefinition("Basic-Authentication", new OpenApiSecurityScheme
                {
                    Type = SecuritySchemeType.Http,
                    Scheme = "basic",
                    Description = "Input your username and password to access this API",
                    Name = "Authorization",
                    In = ParameterLocation.Header
                });

                options.EnableAnnotations();
                options.DocInclusionPredicate((docName, apiDesc) => true);
                options.OperationFilter<AuthorizationOperationFilter>();
                options.OperationFilter<ResponseStatusOperationFilter>();
                options.OperationFilter<RemoveVersionParameters>();
                options.DocumentFilter<FilterVersionsInPaths>();

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml".Replace("IIS", "Route");
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                options.IncludeXmlComments(xmlPath, true);
            };

        private OpenApiInfo GetOpenApiDocument(string apiVersion) => new OpenApiInfo
        {
            Version = $"v{apiVersion}",
            Title = _applicationName + " " + _environmentName + " " + $"V{apiVersion}"
        };
    }

    public class ResponseStatusOperationFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation.Responses == null)
            {
                operation.Responses = new OpenApiResponses();
            }

            if (!operation.Responses.ContainsKey("424"))
            {
                operation.Responses.Add("424", new OpenApiResponse { Description = "Failed Dependency" });
            }

            if (!operation.Responses.ContainsKey("429"))
            {
                operation.Responses.Add("429", new OpenApiResponse { Description = "Too Many Requests" });
            }

            if (!operation.Responses.ContainsKey("500"))
            {
                operation.Responses.Add("500", new OpenApiResponse { Description = "Internal Server Error" });
            }

            if (!operation.Responses.ContainsKey("503"))
            {
                operation.Responses.Add("503", new OpenApiResponse { Description = "Service Unavailable" });
            }
        }
    }

    public class AuthorizationOperationFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation == null)
            {
                return;
            }

            if (operation.Parameters == null)
            {
                operation.Parameters = new List<OpenApiParameter>();
            }

            if (context?.MethodInfo == null)
            {
                return;
            }

            var endpointsWithAuthorization = context.MethodInfo
                .GetCustomAttributes(true)
                .OfType<AuthorizeAttribute>()
                .Distinct();

            if (operation.Responses == null)
            {
                operation.Responses = new OpenApiResponses();
            }

            if (endpointsWithAuthorization.Any())
            {
                if (!operation.Responses.ContainsKey("401"))
                {
                    operation.Responses.Add("401", new OpenApiResponse { Description = "Unauthorized" });
                }

                if (!operation.Responses.ContainsKey("403"))
                {
                    operation.Responses.Add("403", new OpenApiResponse { Description = "Forbidden" });
                }

                if (operation.Security == null)
                {
                    operation.Security = new List<OpenApiSecurityRequirement>();
                }

                var basicAuthScheme = new OpenApiSecurityScheme
                {
                    Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "Basic-Authentication" }
                };

                operation.Security = new List<OpenApiSecurityRequirement>
                {
                    new OpenApiSecurityRequirement
                    {
                        [ basicAuthScheme ] = new List<string>()
                    }
                };
            }
        }
    }

    public class SetVersionInPaths : IDocumentFilter
    {
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            if (swaggerDoc?.Paths == null || string.IsNullOrWhiteSpace(swaggerDoc.Info?.Version))
            {
                return;
            }

            var paths = new OpenApiPaths();
            foreach (var openApiPath in swaggerDoc.Paths.Where(openApiPath => !string.IsNullOrWhiteSpace(openApiPath.Key)))
            {
                paths.Add(openApiPath.Key.Replace("v{version}", swaggerDoc.Info.Version), openApiPath.Value);
            }

            swaggerDoc.Paths = paths;
        }
    }

    public class FilterVersionsInPaths : IDocumentFilter
    {

        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            if (swaggerDoc?.Paths == null || string.IsNullOrWhiteSpace(swaggerDoc.Info?.Version))
            {
                return;
            }

            var paths = new OpenApiPaths();
            foreach (var openApiPath in swaggerDoc.Paths.Where(openApiPath => !string.IsNullOrWhiteSpace(openApiPath.Key)))
            {
                var currentVersion = DotnetWebConstants.ApiList().FirstOrDefault(i => openApiPath.Key.ToLowerInvariant().Contains($"v{i}"));

                if (currentVersion == null)
                {
                    paths.Add(openApiPath.Key.Replace("v{version}", $"v{currentVersion}"), openApiPath.Value);
                }
                else
                {
                    var currentContextVersion =
                        context.ApiDescriptions.FirstOrDefault(i => !string.IsNullOrWhiteSpace(i.RelativePath) && i.RelativePath.ToLowerInvariant().Contains($"v{currentVersion}"))?.RelativePath;

                    if (!string.IsNullOrWhiteSpace(currentContextVersion))
                    {
                        paths.Add(openApiPath.Key.Replace("v{version}", $"v{currentVersion}"), openApiPath.Value);
                    }
                }
            }

            swaggerDoc.Paths = paths;
        }
    }

    public class RemoveVersionParameters : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation?.Parameters == null || !operation.Parameters.Any())
            {
                return;
            }

            var hasVersion = operation.Parameters.FirstOrDefault(i => "version".Equals(i.Name, StringComparison.OrdinalIgnoreCase));

            if (hasVersion != null)
            {
                operation.Parameters.Remove(operation.Parameters.Single(p => p.Name == "version"));
            }
        }
    }
}
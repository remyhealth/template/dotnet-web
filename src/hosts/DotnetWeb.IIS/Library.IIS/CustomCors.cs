using System;
using System.Collections.Generic;
using DotnetWeb.Common.Library.Common.ProjectControl;
using Microsoft.AspNetCore.Cors.Infrastructure;

namespace DotnetWeb.IIS.Library.IIS
{
    public class CustomCors
    {
        private readonly ICurrentProjectInformation _currentProjectInformation;
        public const string PolicyName = "cors";

        public CustomCors(ICurrentProjectInformation currentProjectInformation)
        {
            _currentProjectInformation = currentProjectInformation;
        }

        public Action<CorsOptions> Get(string policyName = PolicyName) =>
            options =>
            {
                options.AddPolicy(policyName, GetBuilder().Build());
                options.DefaultPolicyName = policyName;
            };

        public CorsPolicyBuilder GetBuilder()
        {
            var builder = new CorsPolicyBuilder(GetAllowedCorsOrigins());
            builder.AllowCredentials();
            builder.AllowAnyHeader();
            builder.AllowAnyMethod();
            return builder;
        }

        private string[] GetAllowedCorsOrigins()
        {
            var allowedOrigins = new List<string>
            {
                "http://example.com",
                "http://www.contoso.com"
            };

            return allowedOrigins.ToArray();
        }
    }
}
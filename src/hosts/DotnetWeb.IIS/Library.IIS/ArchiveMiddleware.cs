using System.IO;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using DotnetWeb.Bootstrapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.Extensions.DependencyInjection;

namespace DotnetWeb.IIS.Library.IIS
{
    public class ArchiveMiddleware
    {
        private readonly RequestDelegate _next;

        public ArchiveMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            var pipeline = httpContext.RequestServices.GetService<IPipeline>();
            var claimsPrincipal = httpContext.RequestServices.GetService<ClaimsPrincipal>();
            if (pipeline != null && claimsPrincipal != null)
            {
                var requestPath = httpContext.Request.Path.HasValue ? httpContext.Request.Path.Value : null;
                var requestMethod = httpContext.Request.Method;
                var requestHeaders = httpContext.Request.Headers;

                var requestCanProceed = RequestCanProceed(httpContext) &&
                                        pipeline.DoBeforePipeline(requestPath, requestMethod, requestHeaders);

                if (requestCanProceed)
                {
                    await BeforeRequest(httpContext.Request);
                }

                var responseCanProceed = ResponseCanProceed(httpContext) &&
                                         pipeline.DoAfterPipeline(requestPath, requestMethod, requestHeaders);

                if (requestCanProceed && responseCanProceed)
                {
                    var originalBodyStream = httpContext.Response.Body;

                    using (var responseBody = new MemoryStream())
                    {
                        httpContext.Response.Body = responseBody;

                        await _next(httpContext);

                        await AfterRequest(httpContext.Response);

                        await responseBody.CopyToAsync(originalBodyStream);
                    }
                }
                else
                {
                    await _next(httpContext);
                }
            }
            else
            {
                await _next(httpContext);
            }
        }

        private async Task BeforeRequest(HttpRequest request)
        {
            var initialBodyBytes = await StreamToByteArray(request.Body);

            var pipeline = request.HttpContext.RequestServices.GetService<IPipeline>();
            await pipeline.BeforeRequest(request.Headers, GetRequestBody(initialBodyBytes));

            request.EnableRewind();
            request.Body = ByteArrayToStream(initialBodyBytes);
        }

        public async Task<byte[]> StreamToByteArray(Stream input)
        {
            using (var ms = new MemoryStream())
            {
                await input.CopyToAsync(ms);
                return ms.ToArray();
            }
        }

        public static MemoryStream ByteArrayToStream(byte[] input) => new MemoryStream(input);

        private async Task AfterRequest(HttpResponse response)
        {
            var body = await GetResponseBody(response.Body);

            var pipeline = response.HttpContext.RequestServices.GetService<IPipeline>();
            await pipeline.AfterRequest(response.Headers, body);
        }

        private string GetRequestBody(byte[] body)
        {
            var retVal = Encoding.UTF8.GetString(body);

            return string.IsNullOrWhiteSpace(retVal) ? null : retVal;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0067:Dispose objects before losing scope", Justification = "<Pending>")]
        private async Task<string> GetResponseBody(Stream responseBody)
        {
            responseBody.Seek(0, SeekOrigin.Begin);

            var body = new StreamReader(responseBody);
            var json = await body.ReadToEndAsync();

            responseBody.Seek(0, SeekOrigin.Begin);

            return string.IsNullOrWhiteSpace(json) ? null : json;
        }

        private bool RequestCanProceed(HttpContext httpContext) => httpContext?.Request != null;

        private bool ResponseCanProceed(HttpContext httpContext) => httpContext?.Response != null;
    }

    public static class ArchiveMiddlewareExtensions
    {
        public static IApplicationBuilder UseArchiveMiddleware(this IApplicationBuilder builder) => builder.UseMiddleware<ArchiveMiddleware>();
    }
}
using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;

namespace DotnetWeb.IIS.Library.IIS
{
    public class CustomApiVersioning
    {
        public Action<ApiVersioningOptions> Get(string currentApiVersion) =>
            option =>
            {
                option.ReportApiVersions = true;
                option.AssumeDefaultVersionWhenUnspecified = true;
                option.DefaultApiVersion = new ApiVersion(int.Parse(currentApiVersion), 0);
            };
    }
}
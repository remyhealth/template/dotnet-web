using System;
using Autofac.Extensions.DependencyInjection;
using DotnetWeb.Common;
using DotnetWeb.Common.Library.Common.EnvironmentControl;
using DotnetWeb.Common.Library.Common.LogControl;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Serilog;

namespace DotnetWeb.IIS
{
    internal class Program
    {
        private static readonly ILogLocationUtility LogLocationUtility;
        private static readonly string ApplicationName;
        private static readonly string DefaultLogLocation;

        static Program()
        {
            LogLocationUtility = new LogLocationUtility(new EnvironmentVariableUtility());
            ApplicationName = DotnetWebConstants.ApplicationName;
            DefaultLogLocation = DotnetWebConstants.DefaultLogLocation;
        }

        public static void Main(string[] args)
        {
            LoggerSetup.Initialize(LogLocationUtility.GetLogLocation(DefaultLogLocation, ApplicationName));

            try
            {
                Log.Information("Started program");
                CreateWebHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Stopped program because of exception");
                throw;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) => WebHost.CreateDefaultBuilder(args)
            .ConfigureServices(services => { services.AddAutofac(); })
            .UseSerilog()
            .UseStartup<Startup>();
    }
}
using System.IO;
using DotnetWeb.AzureFunction;
using DotnetWeb.Bootstrapper.Library.Bootstrapper;
using DotnetWeb.Common;
using DotnetWeb.Common.Library.Common.EnvironmentControl;
using DotnetWeb.Common.Library.Common.LogControl;
using DotnetWeb.Common.Library.Common.ParserControl;
using DotnetWeb.Common.Library.Common.ProjectControl;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Serilog;

[assembly: FunctionsStartup(typeof(Startup))]
namespace DotnetWeb.AzureFunction
{
    internal class Startup : FunctionsStartup
    {
        private static readonly ILogLocationUtility LogLocationUtility;
        private static readonly IEnvironmentVariableUtility EnvironmentVariableUtility;
        private static readonly string DefaultLogLocation;
        private static readonly string CurrentApiVersion;

        static Startup()
        {
            EnvironmentVariableUtility = new EnvironmentVariableUtility();
            LogLocationUtility = new LogLocationUtility(EnvironmentVariableUtility);
            DefaultLogLocation = DotnetWebConstants.DefaultLogLocation;
            CurrentApiVersion = DotnetWebConstants.CurrentApiVersion();
        }

        public override void Configure(IFunctionsHostBuilder functionsHostBuilder)
        {
            var projectInformation = GetCurrentProjectInformation(CurrentApiVersion, EnvironmentVariableUtility);
            var healthInformation = new ProjectInformationMapper().MapHealth(projectInformation);

            LoggerSetup.Initialize(LogLocationUtility.GetLogLocation(DefaultLogLocation, projectInformation.ApplicationName));

            functionsHostBuilder.Services
                .AddSingleton(projectInformation)
                .AddSingleton(healthInformation)
                .AddLogging(builder => builder.AddSerilog());

            var serviceCollection = new BootstrapperRegistration()
                .Register(new ServiceCollection());

            functionsHostBuilder.Services.Add(serviceCollection);
        }

        private IConfiguration GetConfigurationRoot(IConfiguration configuration = null)
        {
            if (configuration != null)
            {
                return new ConfigurationBuilder()
                    .AddConfiguration(configuration)
                    .AddJsonFile("appsettings.json", false, false)
                    .AddEnvironmentVariables().Build();
            }

            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, false)
                .AddEnvironmentVariables().Build();
        }

        private ICurrentProjectInformation GetCurrentProjectInformation(string currentApiVersion, IEnvironmentVariableUtility environmentVariableUtility)
        {
            var appSettings = GetConfigurationRoot()
                .GetSection(AppSettings.AppSettingsSectionName)
                .Get<AppSettings>();

            var environment = new CustomHostingEnvironmentMapper(appSettings.ApplicationName, environmentVariableUtility).Get();

            return new ProjectInformationMapper().MapCurrent(appSettings, currentApiVersion, environment.Environment);
        }
    }
}
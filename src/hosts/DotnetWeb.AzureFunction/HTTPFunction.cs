using System.Linq;
using System.Threading.Tasks;
using DotnetWeb.Common;
using DotnetWeb.Common.Library.Common.ParserControl;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace DotnetWeb.AzureFunction
{
    public class HttpFunction
    {
        private readonly IDotnetWebService _service;
        private readonly IArgumentParser _parser;

        public HttpFunction(IDotnetWebService dotnetWebService, IArgumentParser parser)
        {
            _service = dotnetWebService;
            _parser = parser;
        }

        [FunctionName("HttpFunction")]
        public async Task<IActionResult> Run([HttpTrigger(AuthorizationLevel.Function, "post", Route = null)]
            HttpRequest httpRequest,
            ILogger logger)
        {
            if (_parser.CanParseBody(httpRequest?.Body, out var parsed))
            {
                await _service.Get(parsed.FirstOrDefault());
                return new OkResult();
            }

            return new BadRequestObjectResult("Could not parse request");
        }
    }
}
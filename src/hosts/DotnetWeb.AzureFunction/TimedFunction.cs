using System;
using System.Threading.Tasks;
using DotnetWeb.Common;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;

namespace DotnetWeb.AzureFunction
{
    public class TimedFunction
    {
        private readonly IDotnetWebService _service;

        public TimedFunction(IDotnetWebService dotnetWebService)
        {
            _service = dotnetWebService;
        }

        [FunctionName("TimedFunction")]
        public async Task Run([TimerTrigger("0 0 * * * *")]TimerInfo myTimer, ILogger log)
        {
            log.LogInformation($"Timer trigger function started at: {DateTime.Now}");
            log.LogInformation($"Timer trigger function last executed at: {myTimer?.ScheduleStatus?.Last}");

            try
            {
                await _service.List();
            }
            catch (Exception e)
            {
                log.LogError(e, $"Timer trigger function failed at: {DateTime.Now}");
            }
        }
    }
}
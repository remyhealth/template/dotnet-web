using System;
using DotnetWeb.Common.Library.Common.ProjectControl;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.FileProviders;

namespace DotnetWeb.ConsoleApplication
{
    public class ConsoleHostingEnvironment : IHostingEnvironment
    {
        private readonly string _applicationName;
        private readonly string _environmentName;

        public ConsoleHostingEnvironment(ICurrentProjectInformation currentProjectInformation)
        {
            _applicationName = currentProjectInformation?.ApplicationName;
            _environmentName = currentProjectInformation?.CurrentEnvironment?.Name;
        }

        public string EnvironmentName { get => _environmentName; set => throw new NotSupportedException(); }
        public string ApplicationName { get => _applicationName; set => throw new NotSupportedException(); }
        public string WebRootPath { get; set; }
        public IFileProvider WebRootFileProvider { get; set; }
        public string ContentRootPath { get; set; }
        public IFileProvider ContentRootFileProvider { get; set; }
    }
}
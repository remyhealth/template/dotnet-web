using DotnetWeb.Common.Library.Common.ProjectControl;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DotnetWeb.Route.Controllers
{
    [Authorize]
    [ApiController]
    [ApiVersionNeutral]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class HealthController : Controller
    {
        private readonly IHealthProjectInformation _healthProjectInformation;
        public HealthController(IHealthProjectInformation healthProjectInformation)
        {
            _healthProjectInformation = healthProjectInformation;
        }

        /// <summary>
        /// Health Check Endpoint.
        /// </summary>
        /// <returns>Returns a success statement</returns>
        /// <response code="200">All Good</response>
        [ProducesResponseType(200)]
        [AllowAnonymous]
        [HttpGet]
        public IActionResult Get()
        {
            if (_healthProjectInformation != null)
            {
                return Ok(_healthProjectInformation);
            }

            return NoContent();
        }
    }
}
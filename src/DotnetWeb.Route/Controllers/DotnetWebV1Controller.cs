using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DotnetWeb.Common;
using DotnetWeb.Public;
using DotnetWeb.Route.Library.Route;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.Logging;

namespace DotnetWeb.Route.Controllers
{
    [Authorize]
    [ApiController]
    [ApiVersion(DotnetWebConstants.ApiVersion1)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class DotnetWebV1Controller : BaseController
    {
        private readonly IDotnetWebService _service;
        private readonly ILogger<DotnetWebV1Controller> _logger;

        public DotnetWebV1Controller(IDotnetWebService service, ILogger<DotnetWebV1Controller> logger)
        {
            _service = service;
            _logger = logger;
        }

        /// <summary>
        /// Lists all Items.
        /// </summary>
        /// <returns>A list of all items</returns>
        /// <response code="200">Returns the item list</response>
        [ProducesResponseType(200)]
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Get()
        {
            _logger.LogDebug("List Controller Hit");

            var model = await _service.List();
            return Ok(model);
        }

        /// <summary>
        /// Fetches a specific Item.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A the newly created item</returns>
        /// <response code="200">Returns the item that was created</response>
        /// <response code="400">If the item was not found</response>
        /// <response code="404">If the item was not found</response>
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [HttpGet("{id}")]
        [Authorize]
        public async Task<IActionResult> Get(string id)
        {
            _logger.LogDebug("Get Controller Hit");

            var model = await _service.Get(id);
            if (model != null)
            {
                return Ok(model);
            }

            return NotFound();
        }

        /// <summary>
        /// Creates an item.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///        "name": "Item1"
        ///     }
        ///
        /// </remarks>
        /// <param name="item"></param>
        /// <returns>A a success message</returns>
        /// <response code="201">A message that the item has been created</response>
        [ProducesResponseType(201)]
        [HttpPost]
        [Authorize]
        public IActionResult Post([FromBody]DotnetWebRequest item)
        {
            _logger.LogDebug("Post Controller Hit");

            return Created($"{Request.GetDisplayUrl()}/{item}", item);
        }

        /// <summary>
        /// Update an item.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///        "id": 1,
        ///        "name": "Item1"
        ///     }
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="item"></param>
        /// <returns>A a success message</returns>
        /// <response code="201">A message that the item has been updated</response>
        /// <response code="404">If the item was not found</response>
        [ProducesResponseType(201)]
        [ProducesResponseType(404)]
        [HttpPut("{id}")]
        [Authorize]
        public IActionResult Put(int id, [FromBody]DotnetWebRequest item)
        {
            _logger.LogDebug("Put Controller Hit");

            return Accepted();
        }

        /// <summary>
        /// Delete an item.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///        "id": 1
        ///     }
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>A a success message</returns>
        /// <response code="201">A message that the item has been deleted</response>
        /// <response code="404">If the item was not found</response>
        [ProducesResponseType(201)]
        [ProducesResponseType(404)]
        [HttpDelete("{id}")]
        [Authorize]
        public IActionResult Delete(int id)
        {
            _logger.LogDebug("Delete Controller Hit");

            return Accepted();
        }
    }
}
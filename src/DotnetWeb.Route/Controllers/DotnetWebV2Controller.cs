using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DotnetWeb.Common;
using DotnetWeb.Public;
using DotnetWeb.Route.Library.Route;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.Logging;

namespace DotnetWeb.Route.Controllers
{
    [Authorize]
    [ApiController]
    [ApiVersion(DotnetWebConstants.ApiVersion2)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class DotnetWebV2Controller : BaseController
    {
        private readonly IDotnetWebService _service;
        private readonly ILogger<DotnetWebV1Controller> _logger;

        public DotnetWebV2Controller(IDotnetWebService service, ILogger<DotnetWebV1Controller> logger)
        {
            _service = service;
            _logger = logger;
        }

        /// <summary>
        /// Lists all Items.
        /// </summary>
        /// <returns>A list of all items</returns>
        /// <response code="200">Returns the item list</response>
        [ProducesResponseType(200)]
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Get()
        {
            _logger.LogDebug("List Controller Hit");

            var model = await _service.List();
            return Ok(model);
        }



        /// <summary>
        /// Creates an item.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///        "name": "Item1"
        ///     }
        ///
        /// </remarks>
        /// <param name="item"></param>
        /// <returns>A a success message</returns>
        /// <response code="201">A message that the item has been created</response>
        [ProducesResponseType(201)]
        [HttpPost]
        [Authorize]
        public IActionResult Post([FromBody]DotnetWebRequest item)
        {
            _logger.LogDebug("Post Controller Hit");

            return Created($"{Request.GetDisplayUrl()}/{item}", item);
        }
    }
}
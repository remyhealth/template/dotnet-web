using System.Data.SqlClient;
using DotnetWeb.Common.Library.Common.DatabaseControl;

namespace DotnetWeb.Repository.Library.Repository
{
    public class SqlAzureConnection : IConnection
    {
        public SqlAzureConnection()
        {
            ConnectionString = string.Empty;
        }

        public SqlAzureConnection(string datasource, string username, string password, string catalog, bool encrypt = true)
        {
            var builder = new SqlConnectionStringBuilder
            {
                DataSource = datasource,
                UserID = username,
                Password = password,
                InitialCatalog = catalog,
                Encrypt = encrypt,
                TrustServerCertificate = false
            };
            ConnectionString = builder.ConnectionString;
        }

        public SqlAzureConnection(string datasource, string username, string password, string catalog, bool isReadOnly, bool encrypt = true)
        {
            var builder = new SqlConnectionStringBuilder
            {
                DataSource = datasource,
                UserID = username,
                Password = password,
                InitialCatalog = catalog,
                Encrypt = encrypt,
                TrustServerCertificate = false,
                ApplicationIntent = isReadOnly ? ApplicationIntent.ReadOnly : ApplicationIntent.ReadWrite
            };
            ConnectionString = builder.ConnectionString;
        }

        public SqlAzureConnection(SqlConnectionStringBuilder builder, bool encrypt = true)
        {
            builder.Encrypt = encrypt;
            builder.TrustServerCertificate = false;
            ConnectionString = builder.ConnectionString;
        }

        public string ConnectionString { get; }
    }
}
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DotnetWeb.Common.Library.Common.DatabaseControl;

namespace DotnetWeb.Repository.Library.Repository
{
    public abstract class BaseRepository
    {
        private readonly IConnection _connection;
        private const int DefaultCommandTimeoutInSeconds = 5;

        protected BaseRepository(IConnection connection)
        {
            _connection = connection;
        }

        protected async Task<T> WithConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            ValidateConnection();
            try
            {
                return await ExecuteWithoutBuffer(getData);
            }
            catch (TimeoutException ex)
            {
                throw HandleTimeoutException(ex);
            }
            catch (SqlException ex)
            {
                throw HandleSqlException("WithConnection()", ex);
            }
        }

        protected T WithSynchronousConnection<T>(Func<IDbConnection, T> getData)
        {
            ValidateConnection();
            try
            {
                return ExecuteSynchronousWithoutBuffer(getData);
            }
            catch (TimeoutException ex)
            {
                throw HandleTimeoutException(ex);
            }
            catch (SqlException ex)
            {
                throw HandleSqlException("WithSynchronousConnection()", ex);
            }
        }

        protected async Task<TResult> WithConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData,
            Func<TRead, Task<TResult>> process)
        {
            ValidateConnection();
            try
            {
                return await ExecuteWithBuffer(getData, process);
            }
            catch (TimeoutException ex)
            {
                throw HandleTimeoutException(ex);
            }
            catch (SqlException ex)
            {
                throw HandleSqlException("WithConnection()", ex);
            }
        }

        protected string WrapForLike(string val)
        {
            if (!string.IsNullOrWhiteSpace(val))
            {
                return "%" + EncodeForLike(val) + "%";
            }

            return null;
        }

        protected string EncodeForLike(string val)
        {
            if (!string.IsNullOrWhiteSpace(val))
            {
                return val.Replace("[", "[[]").Replace("%", "[%]");
            }

            return "";
        }

        protected async Task<T> Get<T>(string sql, object param = null, int timeout = DefaultCommandTimeoutInSeconds)
        {
            if (string.IsNullOrWhiteSpace(sql))
            {
                return default(T);
            }

            if (param == null)
            {
                param = new { };
            }

            return await WithConnection(async c =>
            {
                var response =
                    await c.QueryAsync<T>(
                        sql, param, null, timeout);

                if (response != null)
                {
                    return response.FirstOrDefault();
                }

                return default(T);
            });
        }

        protected async Task<IEnumerable<T>> List<T>(string sql, object param = null, int timeout = DefaultCommandTimeoutInSeconds)
        {
            if (string.IsNullOrWhiteSpace(sql))
            {
                return new List<T>();
            }

            if (param == null)
            {
                param = new { };
            }

            return await WithConnection(async c =>
            {
                var response =
                    await c.QueryAsync<T>(
                        sql, param, null, timeout);

                return response ?? new List<T>();
            });
        }

        protected async Task<T> Insert<T>(string sql, object param = null, int timeout = DefaultCommandTimeoutInSeconds)
        {
            if (string.IsNullOrWhiteSpace(sql))
            {
                return default(T);
            }

            if (param == null)
            {
                param = new { };
            }

            return await WithConnection(async c =>
            {
                var response =
                    await c.QueryAsync<T>(
                        sql, param, null, timeout);

                if (response != null)
                {
                    return response.FirstOrDefault();
                }

                return default(T);
            });
        }

        protected async Task Execute(string sql, object param = null, int timeout = DefaultCommandTimeoutInSeconds)
        {
            if (string.IsNullOrWhiteSpace(sql))
            {
                return;
            }

            if (param == null)
            {
                param = new { };
            }

            await WithConnection(async c => await c.ExecuteAsync(sql, param, null, timeout));
        }

        private Exception HandleSqlException(string withConnection, SqlException ex) =>
            new Exception(
                $"{GetType().FullName}.{withConnection} experienced a SQL exception (not a timeout)",
                ex);

        private Exception HandleTimeoutException(Exception ex) =>
            new Exception($"{GetType().FullName}.WithConnection() experienced a SQL timeout",
                ex);

        private T ExecuteSynchronousWithoutBuffer<T>(Func<IDbConnection, T> getData)
        {
            using (var connection = new SqlConnection(_connection.ConnectionString))
            {
                connection.Open();
                return getData(connection);
            }
        }

        private async Task<T> ExecuteWithoutBuffer<T>(Func<IDbConnection, Task<T>> getData)
        {
            using (var connection = new SqlConnection(_connection.ConnectionString))
            {
                await connection.OpenAsync();
                return await getData(connection);
            }
        }

        private async Task<TResult> ExecuteWithBuffer<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData,
            Func<TRead, Task<TResult>> process)
        {
            using (var connection = new SqlConnection(_connection.ConnectionString))
            {
                await connection.OpenAsync();
                var data = await getData(connection);
                return await process(data);
            }
        }

        private void ValidateConnection()
        {
            if (string.IsNullOrWhiteSpace(_connection?.ConnectionString))
            {
                throw new Exception("MissingDatabaseConnection");
            }

            var builder = new SqlConnectionStringBuilder(_connection.ConnectionString);

            if (string.IsNullOrWhiteSpace(builder.DataSource))
            {
                throw new Exception("MissingDatabaseConnection-Url");
            }

            if (string.IsNullOrWhiteSpace(builder.UserID))
            {
                throw new Exception("MissingDatabaseConnection-Username");
            }

            if (string.IsNullOrWhiteSpace(builder.Password))
            {
                throw new Exception("MissingDatabaseConnection-Password");
            }

            if (string.IsNullOrWhiteSpace(builder.InitialCatalog))
            {
                throw new Exception("MissingDatabaseConnection-Catalog");
            }
        }
    }
}
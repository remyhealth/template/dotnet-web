using System.Collections.Generic;
using System.Threading.Tasks;
using DotnetWeb.Common;
using DotnetWeb.Common.Library.Common.DatabaseControl;
using DotnetWeb.Public;
using DotnetWeb.Repository.Library.Repository;
using Microsoft.Extensions.Logging;

namespace DotnetWeb.Repository
{
    public class DotnetWebRepository : BaseRepository, IDotnetWebRepository
    {
        private readonly ILogger<DotnetWebRepository> _logger;

        public DotnetWebRepository(IConnection connection, ILogger<DotnetWebRepository> logger) : base(connection)
        {
            _logger = logger;
        }

        public async Task<DotnetWebResponse> Get(int id)
        {
            _logger.LogDebug("Get Repository Hit");

            try
            {
                var sql = @"SELECT * FROM MyTable WHERE Id = @Key";
                return await Get<DotnetWebResponse>(sql, new
                {
                    Key = id
                });
            }
            catch
            {
                return null;
            }
        }

        public async Task<IEnumerable<DotnetWebResponse>> List()
        {
            _logger.LogDebug("List Repository Hit");

            try
            {
                var sql = @"SELECT * FROM MyTable";
                return await List<DotnetWebResponse>(sql);
            }
            catch
            {
                return new List<DotnetWebResponse>();
            }
        }
    }
}
using System.Collections.Generic;
using DotnetWeb.Bootstrapper.Library.Bootstrapper;
using DotnetWeb.Common;
using DotnetWeb.Common.Library.Common.CacheControl;
using DotnetWeb.Common.Library.Common.DatabaseControl;
using DotnetWeb.Common.Library.Common.EnvironmentControl;
using DotnetWeb.Common.Library.Common.ParserControl;
using DotnetWeb.Core;
using DotnetWeb.Repository;
using DotnetWeb.Repository.Library.Repository;
using Microsoft.Extensions.DependencyInjection;

namespace DotnetWeb.Bootstrapper
{
    public class Registrations
    {
        //https://docs.microsoft.com/en-us/aspnet/core/fundamentals/dependency-injection?view=aspnetcore-2.2#service-lifetimes
        public static IList<ServiceDescriptor> Load() => Load(new ServiceCollection());

        public static IList<ServiceDescriptor> Load(IServiceCollection services)
        {
            services.AddSingleton<IConnection>(GetSqlAzureConnection(new EnvironmentVariableUtility()));
            services.AddSingleton<IInstanceCache>(new InstanceCache());
            services.AddSingleton<IArgumentParser>(new ArgumentParser());
            services.AddSingleton<IConsoleRunner, ConsoleRunner>();
            services.AddSingleton<IRunner, DotnetWebRunner>();
            services.AddSingleton<IPipeline, Pipeline>();
            services.AddTransient<IDotnetWebRepository, DotnetWebRepository>();
            services.AddTransient<IDotnetWebService, DotnetWebService>();

            return services;
        }

        private static SqlAzureConnection GetSqlAzureConnection(IEnvironmentVariableUtility environmentVariableUtility) =>
            new SqlAzureConnection(environmentVariableUtility.SafeGet(DotnetWebConstants.DatabaseEnvironmentVariableUrl, ""), environmentVariableUtility.SafeGet(DotnetWebConstants.DatabaseEnvironmentVariableUsername, ""),
                environmentVariableUtility.SafeGet(DotnetWebConstants.DatabaseEnvironmentVariablePassword, ""), environmentVariableUtility.SafeGet(DotnetWebConstants.DatabaseEnvironmentVariableCatalog, ""), false, true);
    }
}
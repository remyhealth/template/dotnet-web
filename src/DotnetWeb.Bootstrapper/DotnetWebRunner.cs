using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotnetWeb.Bootstrapper.Library.Bootstrapper;
using DotnetWeb.Common;
using DotnetWeb.Common.Library.Common.ParserControl;

namespace DotnetWeb.Bootstrapper
{
    public class DotnetWebRunner : IRunner
    {
        private readonly IDotnetWebService _service;
        private readonly IArgumentParser _argumentParser;

        public DotnetWebRunner(IDotnetWebService service, IArgumentParser argumentParser)
        {
            _service = service;
            _argumentParser = argumentParser;
        }

        public async Task Run(IList<string> args)
        {
            if (!_argumentParser.CanParseBody(args, out var parsed))
            {
                return;
            }

            var item = parsed.FirstOrDefault();
            await _service.Get(item);
        }
    }
}
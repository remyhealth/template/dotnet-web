using System;
using System.Collections.Generic;
using DotnetWeb.Common.Library.Common.CacheControl;

namespace DotnetWeb.Bootstrapper.Library.Bootstrapper
{
    public class InstanceCache : IInstanceCache
    {
        private readonly IDictionary<string, Tuple<DateTime, object>> _cacheList;

        public InstanceCache()
        {
            _cacheList = new Dictionary<string, Tuple<DateTime, object>>();
        }

        public bool Exists(string key) => Get<object>(key) != null;

        public T Get<T>(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                return default(T);
            }

            foreach (var pair in _cacheList)
            {
                if (!key.Equals(pair.Key, StringComparison.OrdinalIgnoreCase))
                {
                    continue;
                }

                if (pair.Value?.Item1 == null || pair.Value.Item2 == null)
                {
                    Remove(pair.Key);
                    return default(T);
                }

                if (pair.Value.Item1 < DateTime.UtcNow)
                {
                    Remove(pair.Key);
                    return default(T);
                }

                return (T)pair.Value.Item2;
            }

            return default(T);
        }

        public void Add<T>(string key, T val, TimeSpan expires)
        {
            if (string.IsNullOrWhiteSpace(key) || val == null)
            {
                return;
            }

            if (!Exists(key))
            {
                _cacheList.Add(key, new Tuple<DateTime, object>(DateTime.UtcNow.Add(expires), val));
            }
            else
            {
                _cacheList.Remove(key);
                _cacheList.Add(key, new Tuple<DateTime, object>(DateTime.UtcNow.Add(expires), val));
            }
        }

        public void Remove(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                return;
            }

            _cacheList.Remove(key);
        }
    }
}
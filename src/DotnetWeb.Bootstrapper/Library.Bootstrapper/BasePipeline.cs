using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Primitives;

namespace DotnetWeb.Bootstrapper.Library.Bootstrapper
{
    public abstract class BasePipeline
    {
        protected List<string> HttpMethodList() => new List<string> { "get", "post", "put", "delete" };

        protected List<string> HttpContentTypeList() =>
            new List<string>
            {
                "html"
            };

        protected List<string> HttpAcceptList() =>
            new List<string>
            {
                "application/json",
                "application/xml"
            };

        protected List<string> HttpPathList() =>
            new List<string>
            {
                "health",
                "login",
                "ping",
                "index",
                "authorize",
                "swagger"
            };

        protected List<string> HttpFileTypeExtensionList() =>
            new List<string>
            {
                ".html",
                ".ico",
                ".css",
                ".svg",
                ".jpg",
                ".jpeg",
                ".png",
                ".gif",
                ".js",
                ".wof"
            };

        protected virtual bool IsAcceptableMethod(string httpMethod)
        {
            if (string.IsNullOrWhiteSpace(httpMethod))
            {
                return false;
            }

            return HttpMethodList().Contains(httpMethod.ToLower());
        }

        protected virtual bool IsAcceptableContentType(string contentType)
        {
            if (string.IsNullOrWhiteSpace(contentType))
            {
                return true;
            }

            return !FirstParamContainsSecond(contentType, HttpContentTypeList());
        }

        protected virtual bool IsAcceptableAccept(string accept)
        {
            if (string.IsNullOrWhiteSpace(accept))
            {
                return true;
            }

            return FirstParamContainsSecond(HttpAcceptList(), accept);
        }

        protected virtual bool IsAcceptablePath(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                return true;
            }

            return !FirstParamContainsSecond(path, HttpPathList());
        }

        protected virtual bool IsAcceptableFileTypeExtension(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                return true;
            }

            return !FirstParamContainsSecond(path, HttpFileTypeExtensionList());
        }

        protected bool FirstParamContainsSecond(string val, List<string> list)
        {
            if (string.IsNullOrWhiteSpace(val))
            {
                return false;
            }

            var fixedVal = val.Trim().ToLower();
            return list.FirstOrDefault(i => fixedVal.Contains(i.ToLower())) != null;
        }

        protected bool FirstParamContainsSecond(List<string> list, string val)
        {
            if (string.IsNullOrWhiteSpace(val))
            {
                return false;
            }

            var fixedVal = val.Trim().ToLower();
            return list.FirstOrDefault(i => i.ToLower().Contains(fixedVal)) != null;
        }

        protected string GetHeader(string key, IDictionary<string, StringValues> headers)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                return null;
            }

            if (headers == null || !headers.ContainsKey(key))
            {
                return null;
            }

            if (!headers.TryGetValue(key, out var val))
            {
                return null;
            }

            return val.FirstOrDefault(i => !string.IsNullOrWhiteSpace(i));
        }
    }
}
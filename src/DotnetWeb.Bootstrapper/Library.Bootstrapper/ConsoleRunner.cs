using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace DotnetWeb.Bootstrapper.Library.Bootstrapper
{
    public class ConsoleRunner : IConsoleRunner
    {
        private readonly ILogger<ConsoleRunner> _logger;

        public ConsoleRunner(ILogger<ConsoleRunner> logger)
        {
            _logger = logger;
        }

        public async Task Run(IList<string> args, IRunner runner)
        {
            try
            {
                await runner.Run(args);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error with runner");
            }
        }
    }
}
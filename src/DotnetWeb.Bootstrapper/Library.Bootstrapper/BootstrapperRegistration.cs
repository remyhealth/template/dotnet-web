using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace DotnetWeb.Bootstrapper.Library.Bootstrapper
{
    public class BootstrapperRegistration
    {
        public ContainerBuilder RegisterAutofac(IServiceCollection services)
        {
            var builder = new ContainerBuilder();
            builder.Populate(services);
            builder.RegisterModule(new DotnetWebAutofacModule());
            return builder;
        }

        public IServiceCollection Register(IServiceCollection services) => new ServiceCollection
        {
            Registrations.Load(services)
        };
    }
}
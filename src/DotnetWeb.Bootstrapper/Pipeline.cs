using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DotnetWeb.Bootstrapper.Library.Bootstrapper;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
#pragma warning disable 1998

namespace DotnetWeb.Bootstrapper
{
    public interface IPipeline
    {
        bool DoBeforePipeline(string path, string method, IDictionary<string, StringValues> headers);
        bool DoAfterPipeline(string path, string method, IDictionary<string, StringValues> headers);
        Task BeforeRequest(IDictionary<string, StringValues> headers, string body);
        Task AfterRequest(IDictionary<string, StringValues> headers, string body);
    }

    public class Pipeline : BasePipeline, IPipeline
    {
        private readonly ILogger<Pipeline> _logger;

        public Pipeline(ILogger<Pipeline> logger)
        {
            _logger = logger;
        }

        public bool DoBeforePipeline(string path, string method, IDictionary<string, StringValues> headers)
        {
            if (string.IsNullOrWhiteSpace(path) || string.IsNullOrWhiteSpace(method))
            {
                return false;
            }

            return IsAcceptableMethod(method)
                   && IsAcceptablePath(path)
                   && IsAcceptableFileTypeExtension(path)
                   && IsAcceptableContentType(GetHeader("Content-Type", headers))
                   && IsAcceptableAccept(GetHeader("Accept", headers));
        }

        public bool DoAfterPipeline(string path, string method, IDictionary<string, StringValues> headers)
        {
            if (string.IsNullOrWhiteSpace(path) || string.IsNullOrWhiteSpace(method))
            {
                return false;
            }

            return IsAcceptableMethod(method)
                   && IsAcceptablePath(path)
                   && IsAcceptableFileTypeExtension(path)
                   && IsAcceptableContentType(GetHeader("Content-Type", headers));
        }

        public async Task BeforeRequest(IDictionary<string, StringValues> headers, string body)
        {
            _logger.LogTrace($"Before request {DateTime.UtcNow}");

            if (!string.IsNullOrWhiteSpace(body))
            {
                //TODO: do stuff
            }

            TraceLog(body, "Request");
        }

        public async Task AfterRequest(IDictionary<string, StringValues> headers, string body)
        {
            _logger.LogTrace($"After request {DateTime.UtcNow}");

            if (!string.IsNullOrWhiteSpace(body))
            {
                //TODO: do stuff
            }

            TraceLog(body, "Response");
        }

        private void TraceLog(string body, string messageType)
        {
            if (!string.IsNullOrWhiteSpace(body) && !"[null]".Equals(body))
            {
                _logger.LogTrace("Request: " + body);
            }
            else
            {
                _logger.LogTrace($"Empty {messageType} Body");
            }
        }
    }
}
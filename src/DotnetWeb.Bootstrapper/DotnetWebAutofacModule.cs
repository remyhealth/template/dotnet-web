using Autofac;
using Autofac.Extensions.DependencyInjection;

namespace DotnetWeb.Bootstrapper
{
    public class DotnetWebAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder) => builder.Populate(Registrations.Load());
    }
}